export const featureCollection = `<?xml version='1.0' encoding='UTF-8'?>
<wfs:FeatureCollection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd http://www.deegree.org/app http://geodienste.hamburg.de/HH_WFS_Krankenhaeuser?SERVICE=WFS&amp;VERSION=1.1.0&amp;REQUEST=DescribeFeatureType&amp;OUTPUTFORMAT=text%2Fxml%3B+subtype%3Dgml%2F3.1.1&amp;TYPENAME=app:krankenhaeuser_hh&amp;NAMESPACE=xmlns(app=http%3A%2F%2Fwww.deegree.org%2Fapp)" xmlns:wfs="http://www.opengis.net/wfs" timeStamp="2017-08-30T12:31:07Z" xmlns:gml="http://www.opengis.net/gml">
  <gml:featureMember>
    <app:krankenhaeuser_hh xmlns:app="http://www.deegree.org/app" gml:id="APP_KRANKENHAEUSER_HH_9">
      <app:kh_nummer>20</app:kh_nummer>
      <app:name>Evangelisches Krankenhaus Alsterdorf</app:name>
      <app:strasse>Bodelschwinghstraße 24</app:strasse>
      <app:ort>22337  Hamburg</app:ort>
      <app:homepage>http://www.evangelisches-krankenhaus-alsterdorf.de</app:homepage>
      <app:krankenhausverzeichnis>www.krankenhausverzeichnis.de|www.google.com|www.hamburg.de</app:krankenhausverzeichnis>
      <app:anzahl_planbetten>252</app:anzahl_planbetten>
      <app:hinweiszeile></app:hinweiszeile>
      <app:anzahl_plaetze_teilstationaer>43</app:anzahl_plaetze_teilstationaer>
      <app:teilnahme_notversorgung>false</app:teilnahme_notversorgung>
      <app:teilnahme_geburtsklinik>Nein</app:teilnahme_geburtsklinik>
      <app:geburtsklinik_differenziert>Nein</app:geburtsklinik_differenziert>
      <app:stand>01.01.2016</app:stand>
      <app:geom>
        <!--Inlined geometry 'APP_KRANKENHAEUSER_HH_9_APP_GEOM'-->
        <gml:Point gml:id="APP_KRANKENHAEUSER_HH_9_APP_GEOM" srsName="EPSG:25832">
          <gml:pos>567708.612 5941076.513</gml:pos>
        </gml:Point>
      </app:geom>
    </app:krankenhaeuser_hh>
  </gml:featureMember>
  </wfs:FeatureCollection>`;